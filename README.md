Atlassian Security
==============

This repository contains security tools and functionality.
Specifically you will find:

* XXE safe XML parsers inside `atlassian-secure-xml`.
* Password encoder interface and default implementation for Atlassian applications inside `atlassian-password-encoder`.
* Random generator service for producing cryptographically secure random data inside `atlassian-secure-random`.
* Security utility classes inside `atlassian-secure-utils`.


Usage
======

Add atlassian-security to your project as a dependency and check the appropriate Javadoc for the code you intended to use.


Documentation
=============

Documentation can be found at https://docs.atlassian.com/atlassian-security .

Tests
=====

To run the tests simply execute

	mvn clean test


for Atlassian developers builds are found at
[https://ecosystem-bamboo.internal.atlassian.com/browse/AS](https://ecosystem-bamboo.internal.atlassian.com/browse/AS).

Contributors
============

Pull requests, issues and comments welcome. For pull requests:

* Add tests for new features and bug fixes
* Follow the existing style
* Separate unrelated changes into multiple pull requests

See the existing issues for things to start contributing.

For bigger changes, make sure you start a discussion first by creating
an issue and explaining the intended change.

Atlassian requires contributors to sign a Contributor License Agreement,
known as a CLA. This serves as a record stating that the contributor is
entitled to contribute the code/documentation/translation to the project
and is willing to have it used in distributions and derivative works
(or is willing to transfer ownership).

Prior to accepting your contributions we ask that you please follow the appropriate
link below to digitally sign the CLA. The Corporate CLA is for those who are
contributing as a member of an organization and the individual CLA is for
those contributing as an individual.

* [CLA for corporate contributors](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
* [CLA for individuals](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)

License
========

Copyright (c) 2016 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

