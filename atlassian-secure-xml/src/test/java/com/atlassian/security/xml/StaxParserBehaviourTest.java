package com.atlassian.security.xml;
import java.io.StringReader;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;


public class StaxParserBehaviourTest
{
    XMLStreamReader createReader(String input) throws Exception
    {
        XMLInputFactory fac = SecureXmlParserFactory.newXmlInputFactory();

        XMLStreamReader sr = fac.createXMLStreamReader(new StringReader(input));
        return sr;
    }

    static String gatherCharacters(XMLStreamReader sr) throws XMLStreamException
    {
        StringBuilder sb = new StringBuilder();

        while (sr.next() != XMLStreamConstants.END_DOCUMENT)
        {
            if (sr.getEventType() == XMLStreamConstants.CHARACTERS)
            {
                if (sr.hasText())
                {
                    sb.append(sr.getText());
                }
            }
        }

        return sb.toString();
    }

    @Test
    public void parseDocumentExpandsAmpersand() throws Exception
    {
        XMLStreamReader sr = createReader(SampleXmlDocuments.AMPERSAND_DOCUMENT);

        assertEquals("&", gatherCharacters(sr));
    }

    @Test(expected = XMLStreamException.class, timeout = 1000)
    public void parseBillionLaughsDoesNotExhaustTime() throws Exception
    {
        XMLStreamReader sr = createReader(SampleXmlDocuments.BILLION_LAUGHS);

        while (sr.next() != XMLStreamConstants.END_DOCUMENT);
    }

    @Test(expected = XMLStreamException.class)
    public void externalEntityIsNotRead() throws Exception
    {
        XMLStreamReader sr = createReader(SampleXmlDocuments.externalResourceEntity());

        gatherCharacters(sr);
    }

    @Test
    public void dtdUriPointsToFile() throws Exception
    {
        XMLStreamReader sr = createReader(SampleXmlDocuments.EXTERNAL_DTD);

        assertEquals("", gatherCharacters(sr));
    }

    @Test
    public void dtdUriPointsToUrl() throws Exception
    {
        HttpAttemptDetector detector = new HttpAttemptDetector();

        new Thread(detector).start();

        String s = SampleXmlDocuments.externalUrlDtd(detector.getUrl());

        XMLStreamReader sr = createReader(s);

        assertEquals("", gatherCharacters(sr));

        assertFalse("I don't want to see HTTP connection attempts", detector.wasAttempted());
    }

    @Test
    public void externalEntityIsNotReadForExternalParameterEntity() throws Exception
    {
        HttpAttemptDetector detector = new HttpAttemptDetector();

        new Thread(detector).start();

        XMLStreamReader sr = createReader(SampleXmlDocuments.externalParameterEntity(detector.getUrl()));

        try
        {
            gatherCharacters(sr);
        }
        catch (XMLStreamException e)
        {
            // We don't care if parsing succeeds or fails
        }

        assertFalse("I don't want to see HTTP connection attempts", detector.wasAttempted());
    }
}
