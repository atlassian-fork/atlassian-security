package com.atlassian.security.hsts;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HTTPStringTransportSecurityFilterTest {

    private HTTPStrictTransportSecurityFilter filter;

    @Mock
    private FilterConfig config;
    @Mock
    private HttpServletRequest req;
    @Mock
    private HttpServletResponse res;
    @Mock
    private FilterChain mockChain;

    @Before
    public void setUp() throws Exception {
        filter = new HTTPStrictTransportSecurityFilter();
    }

    @Test
    public void doFilterWithEmptyConfig() throws Exception {
        filter.init(config);
        filter.doFilter(req, res, mockChain);
        verify(res).setHeader(eq("Strict-Transport-Security"), eq("max-age=31536000"));
    }

    @Test
    public void doFilterWithMaxAge() throws Exception {
        when(config.getInitParameter("max-age")).thenReturn("3600");
        filter.init(config);
        filter.doFilter(req, res, mockChain);
        verify(res).setHeader(eq("Strict-Transport-Security"), eq("max-age=3600"));
    }

    @Test
    public void doFilterWithPreload() throws Exception {
        when(config.getInitParameter("preload")).thenReturn("True");
        filter.init(config);
        filter.doFilter(req, res, mockChain);
        verify(res).setHeader(eq("Strict-Transport-Security"), eq("max-age=31536000;preload"));
    }

    @Test
    public void doFilterWithPreloadOff() throws Exception {
        when(config.getInitParameter("preload")).thenReturn("false");
        filter.init(config);
        filter.doFilter(req, res, mockChain);
        verify(res).setHeader(eq("Strict-Transport-Security"), eq("max-age=31536000"));
    }

    @Test
    public void doFilterWithIncludeSubdomain() throws Exception {
        when(config.getInitParameter("includeSubDomains")).thenReturn("true");
        filter.init(config);
        filter.doFilter(req, res, mockChain);
        verify(res).setHeader(eq("Strict-Transport-Security"), eq("max-age=31536000;includeSubDomains"));
    }

    @Test
    public void doFilterWithAllInitParams() throws Exception {
        when(config.getInitParameter("max-age")).thenReturn("2592000");
        when(config.getInitParameter("includeSubDomains")).thenReturn("TRUE");
        when(config.getInitParameter("preload")).thenReturn("True");
        filter.init(config);
        filter.doFilter(req, res, mockChain);
        verify(res).setHeader(eq("Strict-Transport-Security"), eq("max-age=2592000;includeSubDomains;preload"));
    }

    @Test
    public void skipFilter() throws Exception {
        when(req.getAttribute("skip!Strict-Transport-Security")).thenReturn(Boolean.TRUE);
        filter.init(config);
        filter.doFilter(req, res, mockChain);
        verify(res, never()).setHeader(eq("Strict-Transport-Security"), any());
    }
}