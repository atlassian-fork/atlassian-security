package com.atlassian.security;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class AtlassianSecurityHeadersFilterTest {

    private AtlassianSecurityHeadersFilter filter;

    @Mock
    private FilterConfig config;
    @Mock
    private HttpServletRequest req;
    @Mock
    private HttpServletResponse res;
    @Mock
    private FilterChain mockChain;

    @Before
    public void setUp() throws Exception {
        filter = new AtlassianSecurityHeadersFilter();
    }

    @Test
    public void doFilter() throws Exception {
        filter.init(config);
        filter.doFilter(req, res, mockChain);
        verify(res).setHeader(eq("X-XSS-Protection"), eq("1; mode=block"));
        verify(res).setHeader(eq("Content-Security-Policy-Report-Only"), anyString());
        verify(res).setHeader(eq("Strict-Transport-Security"), eq("max-age=31536000"));
        verify(res).setHeader(eq("X-Frame-Options"), eq("Deny"));
        verify(res).setHeader(eq("X-Content-Type-Options"), eq("nosniff"));
    }
}