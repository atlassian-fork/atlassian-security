package com.atlassian.security.csp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ContentSecurityPolicyHeaderFilterTest {

    @Mock
    private FilterConfig config;
    @Mock
    private HttpServletRequest req;
    @Mock
    private HttpServletResponse res;
    @Mock
    private FilterChain mockChain;

    private ContentSecurityPolicyHeaderFilter cspFilter;

    @Before
    public void setUp() throws Exception {
         cspFilter = new ContentSecurityPolicyHeaderFilter();
    }

    @Test
    public void doFilterWithEmptyConfig() throws Exception {
        cspFilter.init(config);
        cspFilter.doFilter(req, res, mockChain);

        verify(res).setHeader(eq("Content-Security-Policy-Report-Only"), anyString());
    }

    @Test
    public void doFilterWithEnforceConfig() throws Exception {
        when(config.getInitParameter("report-only")).thenReturn("false");

        cspFilter.init(config);
        cspFilter.doFilter(req, res, mockChain);

        verify(res).setHeader(eq("Content-Security-Policy"), anyString());
    }

    @Test
    public void doFilterWithDifferentPolicy() throws Exception {
        when(config.getInitParameter("policy")).thenReturn("test");

        cspFilter.init(config);
        cspFilter.doFilter(req, res, mockChain);

        verify(res).setHeader("Content-Security-Policy-Report-Only", "test");
    }

    @Test
    public void doFilterWithAttribute() throws Exception {
        when(config.getInitParameter("policy")).thenReturn("script-src nonce-%s;");
        when(req.getAttribute("cspNonce")).thenReturn(("test"));

        cspFilter.init(config);
        cspFilter.doFilter(req, res, mockChain);

        verify(res).setHeader("Content-Security-Policy-Report-Only", "script-src nonce-test;");
    }



}