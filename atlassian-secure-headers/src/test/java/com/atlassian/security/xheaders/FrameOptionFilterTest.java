package com.atlassian.security.xheaders;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FrameOptionFilterTest {

    private FrameOptionsFilter filter;

    @Mock
    private FilterConfig config;
    @Mock
    private HttpServletRequest req;
    @Mock
    private HttpServletResponse res;
    @Mock
    private FilterChain mockChain;

    @Before
    public void setUp() throws Exception {
        filter = new FrameOptionsFilter();
    }

    @Test
    public void doFilterWithEmptyConfig() throws Exception {
        filter.init(config);
        filter.doFilter(req, res, mockChain);
        verify(res).setHeader(eq("X-Frame-Options"), eq("Deny"));
    }

    @Test
    public void doFilterWithSameOrigin() throws Exception {
        when(config.getInitParameter("x-frame-options")).thenReturn("SameOrigin");
        filter.init(config);
        filter.doFilter(req, res, mockChain);
        verify(res).setHeader(eq("X-Frame-Options"), eq("SameOrigin"));
    }

    @Test
    public void doFilterWithURL() throws Exception {
        when(config.getInitParameter("x-frame-options")).thenReturn("http://example.com");
        filter.init(config);
        filter.doFilter(req, res, mockChain);
        verify(res).setHeader(eq("X-Frame-Options"), eq("Allow-from http://example.com"));
    }

    @Test
    public void doFilterWithAnyOtherString() throws Exception {
        when(config.getInitParameter("x-frame-options")).thenReturn("bleep blop bloop");
        filter.init(config);
        filter.doFilter(req, res, mockChain);
        verify(res).setHeader(eq("X-Frame-Options"), eq("Allow-from bleep blop bloop"));
    }

    @Test
    public void skipFilter() throws Exception {
        when(req.getAttribute("skip!X-Frame-Options")).thenReturn(Boolean.TRUE);
        filter.init(config);
        filter.doFilter(req, res, mockChain);
        verify(res, never()).setHeader(eq("X-Frame-Options"), any());
    }
}