package com.atlassian.security.csp;


import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;
import java.util.StringJoiner;

/**
 * <p>
 *  <code>ContentSecurityPolicyHeaderFilter</code> is a filter which generates CSP nonce,
 *  sets it in the request attribute <code>cspNonce</code> and sets the header <code>Content-Security-Policy</code>
 *  for all  HTTP responses.
 * <p>
 * <code>reportOnly</code> parameter enables report only mode for CSP. The default value is <code>true</code>.
 * <p>
 * <code>policy</code> parameter defines Content Security Policy used by the filter. Nonce will replace the "%s" directive. If not specified the default
 *  policy is used.
 */
@WebFilter(asyncSupported = true)
public class ContentSecurityPolicyHeaderFilter implements Filter {

    private final static String NONCE_ATTRIBUTE_NAME = "cspNonce";

    private static Optional<String> reportOnly;
    private static Optional<String> policy;

    public final static String defaultPolicy = new StringJoiner(";")
            .add("object-src 'none'")
            .add("frame-ancestors 'self'")
            .add(new StringJoiner(" ")
                    .add("script-src 'unsafe-eval'")
                    .add(" 'nonce-%s' ")
                    .add("'unsafe-inline' https: http:")
                    .add("'strict-dynamic'").toString())
            .add("base-uri 'self'")
            .add("report-uri https://csp-report-logger.prod.public.atl-paas.net/").toString();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        reportOnly = Optional.ofNullable(filterConfig.getInitParameter("report-only"));
        policy = Optional.ofNullable(filterConfig.getInitParameter("policy"));
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        String cspNonce = (String) request.getAttribute(NONCE_ATTRIBUTE_NAME);
        if (cspNonce == null ) {
            cspNonce = ContentSecurityPolicyHeaderGenerator.generateNonce();
        }
        request.setAttribute(NONCE_ATTRIBUTE_NAME, cspNonce);

        ((HttpServletResponse) response).setHeader(ContentSecurityPolicyHeaderGenerator.generateHeaderName(
                Boolean.valueOf(reportOnly.orElse("true"))),
                ContentSecurityPolicyHeaderGenerator.buildCsp(policy.orElse(defaultPolicy), cspNonce));
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        // nothing to do
    }
}

